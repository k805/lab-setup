1. docker ps. shows running containers
```
docker ps
```
2. If you want to search the images
```
docker search [image-name]
```
3. Pulling the image from docker hub
```
docker pull [image-name]
```
4. display the images in your host
```
docker images
```
5. create container out of image
```
docker create [image-name || image id]
```
6. start the container
```
docker start [container-ID]
```
7. instead of all the  above commands you can run
```
docker run [image-name]
```
8. running the containers in background
```
docker run -d [image-name]
```
9. allocate the port

```
docker run -d -p [host port]:[container port] [image-name]
```
10. How will you enter into container
```
docker exec -it [container ID] [command]
```
11. How to enter when running the container
```
docker run -it [image] [command]
```
